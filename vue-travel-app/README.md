# vue-travel-app

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Open the project in vs code from vue ui
laurent@laurent-HP-ZBook-15-G3:~/workspaces/vue_router/vue-router-tutorial/vue-travel-app$ EDITOR=code vue ui

